
- Link to original repository: https://github.com/regolith-linux/midnight-gtk-theme

# Credits
- This themes base on **Arc-Theme**
- Links : https://github.com/horst3180/arc-theme
- License : GPLv3 (https://choosealicense.com/licenses/gpl-3.0/)

# Installation
- Needed : GTK+ 3.22
- Have been test on : Debian Buster (10), Ubuntu 18.04 LTS, Linux Mint 19
- Install themes : For a better result, Extract Archive File On Directory */usr/share/themes (as root),*
- If you install theme on local system, */.themes or /.local/share/themes* may be GTK2 Theme not working properly.
- Download themes : https://www.opendesktop.org/p/1273208/

## Change themes
- "This is the theme of gtk+ 3 and gtk+ 2. You can use it on Linux distributions in every desktop environment that supports it"
- Debian, Ubuntu (Gnome Desktop) : Use Tweak Tool to change the themes, Gnome Tweak > Appeareance > Themes > Applications
- Linux Mint (Cinnamon): Menu > Settings > Themes > Themes > Controls
